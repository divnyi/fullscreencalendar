//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

struct Constants {
    static let backgroundColor = CGColor(gray: 0.0, alpha: 0.87)

    static let dayCellTodayBgColor = CGColor(red: 0.6, green: 0.4, blue: 1.0, alpha: 1.0)
    static let dayCellDefaultBgColor = CGColor.clear
    static let dayCellInactiveBgColor = CGColor.clear
    
    static let dayCellTodayTextColor = CGColor.black
    static let dayCellDefaultTextColor = CGColor(gray: 0.85, alpha: 1.0)
    static let dayCellInactiveTextColor = CGColor(gray: 0.4, alpha: 1.0)

    static let monthCurrentTextColor = dayCellTodayBgColor
    static let monthDefaultTextColor = CGColor.white
    
    static let dayCellSize = CGFloat(29.0)
}
