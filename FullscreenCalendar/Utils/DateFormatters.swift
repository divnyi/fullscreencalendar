//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

class DateFormatters: NSObject {
    static let shared = DateFormatters()
    
    let month = DateFormatter()
    let day = DateFormatter()
    let weekdayName = DateFormatter()
    let weekdayNameShort = DateFormatter()
    let year = DateFormatter()
    let time = DateFormatter()
    
    override init() {
        self.month.dateFormat = "LLLL"
        self.day.dateFormat = "dd"
        self.weekdayName.dateFormat = "EEEE"
        self.weekdayNameShort.dateFormat = "EEEEE"
        self.year.dateFormat = "yyyy"
        self.time.dateFormat = "HH:mm:ss"
    }
}

extension Date {
    var monthName: String {
        return DateFormatters.shared.month.string(from: self)
    }

    var dayName: String {
        return DateFormatters.shared.day.string(from: self)
    }

    var weekdayName: String {
        return DateFormatters.shared.weekdayName.string(from: self)
    }

    var weekdayNameShort: String {
        return DateFormatters.shared.weekdayNameShort.string(from: self)
    }

    var yearName: String {
        return DateFormatters.shared.year.string(from: self)
    }
    
    var time: String {
        return DateFormatters.shared.time.string(from: self)
    }
}
