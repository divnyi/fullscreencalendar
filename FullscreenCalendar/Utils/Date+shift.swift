//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

extension Date {
    fileprivate var calendar: Calendar { Calendar.current }
    
    func startOfYear() -> Date {
        return calendar.date(from: calendar.dateComponents(
            [.year],
            from: self.startOfDay()
        ))!
    }
    
    func startOfMonth() -> Date {
        return calendar.date(from: calendar.dateComponents(
            [.year, .month],
            from: self.startOfDay()
        ))!
    }
    
    func startOfWeek() -> Date {
        return calendar.date(from: calendar.dateComponents(
            [.yearForWeekOfYear, .weekOfYear],
            from: self.startOfDay()
        ))!
    }
    
    func startOfDay() -> Date {
        return calendar.startOfDay(for: self)
    }
    
    func add(year: Int = 0,
             month: Int = 0,
             week: Int = 0,
             day: Int = 0) -> Date {
        let day = calendar.date(byAdding: DateComponents(year: year,
                                                         month: month,
                                                         day: day,
                                                         weekOfYear: week),
                                to: self)!
        return day
    }

    func thisYear(month: Int = 0) -> Date? {
        let firstDay = calendar.date(byAdding: DateComponents(month: month),
                                     to: self.startOfYear())!
        if firstDay.startOfYear() != self.startOfYear() {
            return nil
        }
        return firstDay
    }

    func thisMonth(week: Int = 0) -> Date? {
        let firstDay = calendar.date(byAdding: DateComponents(weekOfMonth: week),
                                     to: self.startOfMonth().startOfWeek())!
        if week != 0 &&
            firstDay.startOfMonth() != self.startOfMonth() {
            return nil
        }
        return firstDay
    }
    
    func thisWeek(day: Int = 0) -> Date? {
        let day = calendar.date(byAdding: DateComponents(day: day),
                                to: self.startOfWeek())!
        if day.startOfWeek() != self.startOfWeek() {
            return nil
        }
        return day
    }
    
    typealias Week = [Date]
    func wholeWeek() -> Week {
        var idx = 0
        var result: Week = []
        while true {
            if let day = self.thisWeek(day: idx) {
                result.append(day)
                idx += 1
            } else {
                return result
            }
        }
    }

    typealias Month = [Week]
    func wholeMonth() -> Month {
        var idx = 0
        var result: Month = []
        while true {
            if let day = self.thisMonth(week: idx) {
                let week = day.wholeWeek()
                result.append(week)
                idx += 1
            } else {
                return result
            }
        }
    }
    
    typealias Year = [Month]
    func wholeYear() -> Year {
        var idx = 0
        var result: Year = []
        while true {
            if let day = self.thisYear(month: idx) {
                let month = day.wholeMonth()
                result.append(month)
                idx += 1
            } else {
                return result
            }
        }
    }
}
