//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

extension NSWindow {
    enum FadeType {
        case fadeIn
        case fadeOut
        
        var fromAplha: Double {
            switch self {
            case .fadeIn: return 0.0
            case .fadeOut: return 1.0
            }
        }
        var toAplha: Double {
            switch self {
            case .fadeIn: return 1.0
            case .fadeOut: return 0.0
            }
        }
    }
    func fade(_ type: FadeType) {
        self.animationBehavior = .none
        self.alphaValue = CGFloat(type.fromAplha)
        self.makeKeyAndOrderFront(nil)
        NSAnimationContext.runAnimationGroup({ context in
            context.duration = 0.20
            self.animator().alphaValue = CGFloat(type.toAplha)
        }, completionHandler: {
            if type == .fadeOut {
                self.orderOut(nil)
            }
        })
    }
}
