//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa
import HotKey

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    static var shared: AppDelegate {
        return NSApplication.shared.delegate as! AppDelegate
    }
    var window: NSWindow!
    var statusBarItem: NSStatusItem!
    let hotKey = HotKey(key: .f11, modifiers: [])

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        self.showMenuBarExtra()
        self.show()
        self.setupHotkey()
        
        NSApp.activate(ignoringOtherApps: true)
    }
    
    func setupHotkey() {
        hotKey.keyDownHandler = {
            self.toggleVisibility()
        }
    }
    
    func showMenuBarExtra() {
        let statusBar = NSStatusBar.system
        self.statusBarItem = statusBar.statusItem(
            withLength: NSStatusItem.squareLength)
        self.statusBarItem.button?.title = "F11"
        self.statusBarItem.button?.action = #selector(show)
        self.statusBarItem.button?.target = self
    }
    
    var isShown = false
    var vc: OverlayVC?
    @objc func show() {
        if self.window == nil,
            let screen = NSScreen.main {
            let window = Window(contentRect: screen.frame,
                                styleMask: .borderless,
                                backing: .buffered,
                                defer: false)
            self.window = window
            
            self.window.isReleasedWhenClosed = true
            self.window.level = NSWindow.Level(rawValue: NSWindow.Level.RawValue(CGShieldingWindowLevel()))
            self.window.backgroundColor = NSColor.clear
            self.window.isOpaque = false
            self.window.ignoresMouseEvents = false
            
            let vc = OverlayVC.create()
            self.vc = vc
            self.window.contentViewController = vc
            
            window.keyDownFn = { event in
                if let str = event.charactersIgnoringModifiers {
                    let qChar = Int(NSString(string: "q").character(at: 0))
                    let char = Int(NSString(string: str).character(at: 0))
                    switch char {
                    case NSLeftArrowFunctionKey:
                        self.vc?.changeYear(delta: -1)
                    case NSRightArrowFunctionKey:
                        self.vc?.changeYear(delta: 1)
                    case qChar:
                        NSApp.terminate(nil)
                    default:
                        self.hide()
                    }
                }
            }
        }
        
        self.vc?.resetYear()
        
        self.isShown = true
        self.window.fade(.fadeIn)
    }
    
    func hide() {
        self.isShown = false
        self.window.fade(.fadeOut)
    }
    
    func toggleVisibility() {
        if self.isShown {
            self.hide()
        } else {
            self.show()
        }
    }
}

