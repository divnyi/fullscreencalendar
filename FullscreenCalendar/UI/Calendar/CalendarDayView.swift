//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

class CalendarDayView: NSView, NibLoadable {
    var day: CalendarDayVM? {
        didSet {
            self.updateUI()
        }
    }
    static func create(day: CalendarDayVM) -> CalendarDayView {
        let view = CalendarDayView.createFromNib()
        view.day = day
        view.setupUI()
        return view
    }
    static var nibName: String = "CalendarDay"

    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: NSTextField!
    
    func setupUI() {
        self.wantsLayer = true
        self.heightConstraint.constant = Constants.dayCellSize
        self.widthConstraint.constant = Constants.dayCellSize

        self.updateUI()
    }
    
    func updateUI() {
        self.textField?.stringValue = day?.title ?? ""

        if let style = day?.style {
            switch style {
            case .thisDay:
                self.layer?.backgroundColor = Constants.dayCellTodayBgColor
                self.textField?.textColor = NSColor(cgColor: Constants.dayCellTodayTextColor)
            case .normal:
                self.layer?.backgroundColor = Constants.dayCellDefaultBgColor
                self.textField?.textColor = NSColor(cgColor: Constants.dayCellDefaultTextColor)
            case .inactive:
                self.layer?.backgroundColor = Constants.dayCellInactiveBgColor
                self.textField?.textColor = NSColor(cgColor: Constants.dayCellInactiveTextColor)
            }
        }
    }
    
    override func layout() {
        super.layout()
        
        self.layer?.cornerRadius = self.frame.width / 2
    }
}
