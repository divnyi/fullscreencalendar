//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

class CalendarMonthView: NSView, NibLoadable {
    var month: CalendarMonthVM! = nil {
        didSet {
            if let oldValue = oldValue {
                self.updateUI(oldMonth: oldValue)
            }
        }
    }
    static func create(month: CalendarMonthVM) -> CalendarMonthView {
        let view = CalendarMonthView.createFromNib()
        view.month = month
        view.setupUI()
        return view
    }
    static var nibName: String = "CalendarMonth"
    
    @IBOutlet weak var verticalStackView: NSStackView!
    @IBOutlet weak var monthLabel: NSTextField!

    private var dayViews: [[CalendarDayView]] = []
    private func setupUI() {
        self.updateMonth()
        
        for week in [self.month.weekdayRow] + self.month.days {
            let rowViews = week.map { day -> CalendarDayView in
                CalendarDayView.create(day: day)
            }
            let row = NSStackView(views: rowViews)
            row.distribution = .fillEqually
            row.spacing = 0.0

            self.dayViews.append(rowViews)
            self.verticalStackView.addArrangedSubview(row)
        }
    }
    
    private func updateMonth() {
        self.monthLabel.stringValue = self.month.title
        self.monthLabel.textColor = NSColor(cgColor: self.month.isCurrent ? Constants.monthCurrentTextColor : Constants.monthDefaultTextColor)
    }
    
    private func updateUI(oldMonth: CalendarMonthVM) {
        if month.days.count == oldMonth.days.count &&
            month.days.first?.count == oldMonth.days.first?.count {
            self.updateMonth()
            
            for (i, week) in ([self.month.weekdayRow] + self.month.days).enumerated() {
                for (j, day) in week.enumerated() {
                    self.dayViews[i][j].day = day
                }
            }
        } else {
            self.dayViews = []
            _ = self.verticalStackView.arrangedSubviews.map { $0.removeFromSuperview() }
            
            self.setupUI()
        }
    }
}
