//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

class OverlayVC: NSViewController {
    static func create() -> OverlayVC {
        let storyboard = NSStoryboard(name: "Main",
                                      bundle: Bundle.main)
        let vc = storyboard.instantiateController(withIdentifier: "OverlayVC") as! OverlayVC
        return vc
    }

    @IBOutlet weak var timeLabel: NSTextField!
    @IBOutlet weak var dateLabel: NSTextField!
    @IBOutlet weak var yearLabel: NSTextField!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var calendarRow1: NSStackView!
    @IBOutlet weak var calendarRow2: NSStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    func setupUI() {
        self.view.wantsLayer = true
        self.view.layer?.backgroundColor = Constants.backgroundColor

        if let screen = NSScreen.main {
            self.heightConstraint.constant = screen.frame.size.height
            self.widthConstraint.constant = screen.frame.size.width
        }
        
        self.recalculateTimeLabel()
        self.recalculateDateLabel()
        self.redrawCalendars()
        self.setupTimer()
    }
    
    func setupTimer() {
        var currentDay = Date()
        func didDayChange() -> Bool {
            return Date().startOfDay() != currentDay.startOfDay()
        }

        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.recalculateTimeLabel()
            if didDayChange() {
                self.recalculateDateLabel()
                self.redrawCalendars()
            }
        }
    }
    
    func recalculateTimeLabel() {
        let now = Date()
        self.timeLabel.stringValue = now.time
    }
    
    func recalculateDateLabel() {
        let now = Date()
        self.dateLabel.stringValue = "\(now.dayName) \(now.monthName), \(now.yearName)"
    }

    var yearDelta = 0
    var monthViews: [CalendarMonthView] = []
    func redrawCalendars() {
        _ = self.calendarRow1.arrangedSubviews.map { $0.removeFromSuperview() }
        _ = self.calendarRow2.arrangedSubviews.map { $0.removeFromSuperview() }

        let year = CalendarYearVM(yearDelta: self.yearDelta)
        let (firstHalf, secondHalf) = year.months.split()

        for month in firstHalf {
            let view = CalendarMonthView.create(month: month)
            self.monthViews.append(view)
            self.calendarRow1.addArrangedSubview(view)
        }

        for month in secondHalf {
            let view = CalendarMonthView.create(month: month)
            self.monthViews.append(view)
            self.calendarRow2.addArrangedSubview(view)
        }
        
        self.yearLabel.stringValue = year.title
    }
    
    func updateCalendars() {
        let year = CalendarYearVM(yearDelta: self.yearDelta)
        if (year.months.count == self.monthViews.count) {
            for (idx, month) in year.months.enumerated() {
                self.monthViews[idx].month = month
            }
            self.yearLabel.stringValue = year.title
        } else {
            self.redrawCalendars()
        }
    }

    @IBAction func prevYearClicked(_ sender: Any) {
        self.changeYear(delta: -1)
    }
    
    @IBAction func nextYearClicked(_ sender: Any) {
        self.changeYear(delta: 1)
    }
    
    func resetYear() {
        self.yearDelta = 0
        self.updateCalendars()
    }
    
    func changeYear(delta: Int) {
        self.yearDelta += delta
        self.updateCalendars()
    }
    
    @IBAction func backgroundClicked(_ sender: Any) {
        AppDelegate.shared.hide()
    }
    
    @IBAction func quitClicked(_ sender: Any) {
        NSApp.terminate(nil)
    }
}
