//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

struct CalendarMonthVM {
    let title: String
    let isCurrent: Bool
    let weekdayRow: [CalendarDayVM]
    var days: [[CalendarDayVM]] = []
    var emptyRows: Int = 0

    fileprivate let firstDay: Date!
    
    init(month: Date.Month,
         highestWeeksCount: Int) {
        self.firstDay = month.first!.last!.startOfMonth()
        
        self.title = firstDay.monthName
        self.isCurrent = firstDay == Date().startOfMonth()

        self.weekdayRow = firstDay.wholeWeek().map { day -> CalendarDayVM in
            let title = day.weekdayNameShort
            return CalendarDayVM(title: title, style: .normal)
        }

        self.days = month.map(self.weekVM)
        
        self.emptyRows = highestWeeksCount - month.count
        if var lastDay = month.last?.last {
            for _ in 0..<self.emptyRows {
                let nextWeek = lastDay.add(day: 1).wholeWeek()
                lastDay = nextWeek.last!
                let nextWeekVM = weekVM(week: nextWeek)
                self.days.append(nextWeekVM)
            }
        }
    }
    
    func weekVM(week: Date.Week) -> [CalendarDayVM] {
        return week.map { day -> CalendarDayVM in
            CalendarDayVM(date: day, forMonth: firstDay)
        }
    }
}
