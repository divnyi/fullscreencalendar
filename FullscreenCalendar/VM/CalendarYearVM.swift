//
//  This file is part of Fullscreen Calendar.
//
//  Fullscreen Calendar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Fullscreen Calendar is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with Fullscreen Calendar.  If not, see <https://www.gnu.org/licenses/>.
//

import Cocoa

struct CalendarYearVM {
    let title: String
    let months: [CalendarMonthVM]
    
    init(yearDelta: Int) {
        let day = Date().startOfYear().add(year: yearDelta)
        let year = day.wholeYear()
        let firstDay = year.first!.first!.last!.startOfYear()
        
        self.title = firstDay.yearName
        
        var highestWeeksCount = 0
        for month in year {
            highestWeeksCount = max(month.count, highestWeeksCount)
        }
        
        self.months = year.map { month -> CalendarMonthVM in
            return CalendarMonthVM(month: month,
                                   highestWeeksCount: highestWeeksCount)
        }
    }
}
