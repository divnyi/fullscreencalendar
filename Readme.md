# Do you miss dashboard as much as I do?

If you do, then *maybe* you are in the right place.

The feature I miss the most is the ability to quickly see the calendar and time by just pressing some Fn key.

That's why I made this app, which instantly shows date and time by clicking F11 key:

![screenshot](screenshot.png)

# Installation

You can download the repo and build the app yourself, using xcode & cocoapods.

You can also just use the [compiled binary](https://gitlab.com/divnyi/fullscreencalendar/uploads/0e93e389eea3211f4dcb48e70e734088/FullscreenCalendar.zip).
Unzip, move to applications folder.

You would need to go to "System Preferences..." -> "Security & Privacy" and allow the application at the "General" tab, near the "Allow apps downloaded from".

If you want this app to launch at startup, go to "System Preferences..." -> "Users & Groups", "Login Items" tab, add the app there.


# License

    Fullscreen Calendar
    Copyright (C) 2020 Oleksii Horishnii o.divnyi@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
